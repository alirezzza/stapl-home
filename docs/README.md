# STAPL

STAPL is a C++ parallel programming framework designed to work on both shared and distributed memory parallel computers.

Its core is a library of ISO Standard C++ components with interfaces similar to
the (sequential) ISO C++ standard library. STAPL includes a run-time system,
design rules for extending the provided library code, and optimization tools.
Its goal is to allow the user to work at a high level of abstraction and hide
many details specific to parallel programming, to allow a high degree of
productivity, portability, and performance.

## Getting Started

Take a look at the [Getting Started](getting-started.md) guide to learn the basics
about building and running STAPL. Then check out the [tutorial](basic-usage/README.md)
for a brief walkthrough of a basic SGL program.

STAPL is a library, requiring only a C++ compiler, the Boost libraries, and
established communication libraries such as MPI.

## Getting Help

The STAPL development community can be found online at https://gitlab.com/parasol-lab/stapl. If you need more direct support please send email to
[stapl-support@tamu.edu](stapl-support@tamu.edu).

## People

STAPL is designed and maintained by the [Parasol Lab](https://parasol.tamu.edu) at Texas A&M University.
The PIs for this work are [Nancy M. Amato](http://parasol.tamu.edu/~amato) and
[Lawrence Rauchwerger](http://parasol.tamu.edu/~rwerger).

This work has been created with the help of numerous students, post-docs and
research staff. For a full list of contributors, please see our [page](https://parasol.tamu.edu/stapl/).

## Publications

The following articles have been written using the STAPL Graph Library:

* Adam Fidel, Francisco Coral Sabido, Colton Riedel, Nancy Amato, Lawrence Rauchwerger, "Fast Approximate Distance Queries in Unweighted Graphs using Bounded Asynchrony", To appear in Wkshp. on Lang. and Comp. for Par. Comp. (LCPC), Rochester, NY, USA, September 2016.

* **Best Paper Finalist.** Harshvardhan, Adam Fidel, Nancy M. Amato, Lawrence Rauchwerger, "An Algorithmic Approach to Communication Reduction in Parallel Graph Algorithms," In Proc. IEEE Int.Conf. on Parallel Architectures and Compilation Techniques (PACT), San Francisco, CA, USA, November 2015.

* Ioannis Papadopoulos, Nathan Thomas, Adam Fidel, Dielli Hoxha, Nancy M. Amato, Lawrence Rauchwerger, "Asynchronous Nested Parallelism for Dynamic Applications in Distributed Memory," In Wkshp. on Lang. and Comp. for Par. Comp. (LCPC), Raleigh, NC, USA, September 2015.

* Ioannis Papadopoulos, Nathan Thomas, Adam Fidel, Nancy M. Amato, Lawrence Rauchwerger, "STAPL-RTS: An Application Driven Runtime System," In International Conference on Supercomputing (ICS), Newport Beach, California, USA, June 2015.

* Harshvardhan, Brandon West, Adam Fidel, Nancy M. Amato, Lawrence Rauchwerger, "A Hybrid Approach To Processing Big Data Graphs on Memory-Restricted Systems," In Proc. Int. Par. and Dist. Proc. Symp. (IPDPS), Hyderabad, India, May 2015.

*  **Best Paper Award.** Harshvardhan, Adam Fidel, Nancy M. Amato, Lawrence Rauchwerger, "KLA: A New Algorithmic Paradigm for Parallel Graph Computations," In Proc. IEEE Int.Conf. on Parallel Architectures and Compilation Techniques (PACT), Aug 2014.

* Adam Fidel, Sam Ade Jacobs, Shishir Sharma, Nancy M. Amato, Lawrence Rauchwerger, "Using Load Balancing to Scalably Parallelize Sampling-Based Motion Planning Algorithms," In Proc. Int. Par. and Dist. Proc. Symp. (IPDPS), Phoenix, Arizona, USA, May 2014. (pdf, abstract)

* Harshvardhan, Adam Fidel, Nancy M. Amato, Lawrence Rauchwerger, "The STAPL Parallel Graph Library," In Wkshp. on Lang. and Comp. for Par. Comp. (LCPC), Tokyo, Japan, Sep 2012.

* Gabriel Tanase, Antal Buss, Adam Fidel, Harshvardhan, Ioannis Papadopoulos, Olga Pearce, Timmie Smith, Nathan Thomas, Xiabing Xu, Nedhal Mourad, Jeremy Vu, Mauro Bianco, Nancy M. Amato, Lawrence Rauchwerger, "The STAPL Parallel Container Framework," In Proc. ACM SIGPLAN Symp. Prin. Prac. Par. Prog. (PPOPP), Feb 2011.




