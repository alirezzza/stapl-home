# graph_paradigm

Defined in <`stapl/containers/graph/algorithms/paradigms/graph_paradigm.hpp`>

```c++
template<typename WF,typename UF,typename GView,typename PostExecute,typename Predicate>  
size_t graph_paradigm(UF const &,graph_paradigm_params< WF, PostExecute, GView, Predicate > & params)
```

## Summary

The Parallel Graph Algorithm Paradigm.

Allows expression of parallel fine-grained graph algorithms, with optimizations for selecting between different graph algorithmic paradigms.

Selects between the level-synchronous and KLA paradigms based on k.

The user provides a work function to express the computation to be performed on each vertex, and a visitor that will be applied to each neighbor that is visited. The work function is passed in a vertex and a visit object. To visit a neighboring vertex, the work function must call visit(neighbor, visitor()). The work function must return true if the vertex was active (i.e. its value was updated), or false otherwise. The visitor is passed in the target vertex. Visitors may carry state, but must be immutable. They should return true if the visit was successful (i.e. the target vertex will be activated after this visit), or false otherwise. Users may also provide additional functions to be executed after each KLA-Superstep (KLA-SS). 
#### Parameters
* *WF*: The type of the user provided work function expressing computation to be performed over each vertex. 


* *UF*: The type of the user provided visitor expressing computation to be performed over neighboring vertices. 


* *Predicate*: A predicate used by BFS to check for a target vertex. 





#### Parameters
* *uwf*: Functor that implements the operation to be performed on each vertex 


* *post_execute*: Optional functor that will be executed on the graph_view at the end of each KLA-SS. This will be invoked with the input graph_view and the current KLA-SS ID (the ID of the KLA-SS that just finished). 


* *g*: The graph_view over the input graph. 


* *k*: The maximum amount of asynchrony allowed in each phase. 0 <= k <= inf. k == 0 implies level-synchronous paradigm. k >= D implies fully asynchronous paradigm (D is the number of iterations performed by the level-synchronous variant of the algorithm). 


* *predicate*: The predicate to be passed to the level_sync and kla paradigms 





#### Returns
The number of iterations performed by the paradigm.


> Todo: Select between lsync, KLA, async, and adaptive paradigms based on k. -1 (default) can imply adaptive, 0 lsync, >0 KLA, and MAX_INT async.
