# rebalance_global

Defined in <`stapl/containers/graph/algorithms/rebalance_global.hpp`>

```c++
template<typename View,typename Weights>  
void rebalance_global(View const & vw,Weights const & weight_map)
```

## Summary

Parallel global rebalancer based on vertex-weights.

Balances the graph based on vertex-weights by migrating vertices to create an even weight-distribution among the locations. Does one-shot global rebalancing. 
#### Parameters
* *vw*: The graph_view over the input graph. 


* *weight_map*: The vertex property map storing weights for each vertex. 




Invalidates the input graph_view.
