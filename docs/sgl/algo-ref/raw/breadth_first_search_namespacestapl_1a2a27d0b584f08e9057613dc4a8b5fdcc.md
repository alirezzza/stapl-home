# breadth_first_search

Defined in <`stapl/containers/graph/algorithms/breadth_first_search.hpp`>

```c++
template<class GView,class Predicate>  
size_t breadth_first_search(GView & g,typename GView::vertex_descriptor const & source,Predicate const & finish_pred,size_t k)
```

## Summary

A Parallel Breadth-First Search (BFS)

Performs a breadth-first search on the input graph_view, storing the BFS-level and BFS-parent on each reachable vertex. 
#### Parameters
* *g*: The graph_view over the input graph. 


* *source*: The descriptor of the source vertex for this traversal. 


* *finish_pred*: A predicate indicating when to terminate that recieves a vertex. 


* *k*: The maximum amount of asynchrony allowed in each phase. 0 <= k <= inf. k == 0 implies level-synchronous BFS. k >= D implies fully asynchronous BFS (D is diameter of graph). 





#### Returns
The number of iterations performed by the paradigm.
