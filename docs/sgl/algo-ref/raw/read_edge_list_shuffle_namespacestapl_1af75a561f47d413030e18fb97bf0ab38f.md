# read_edge_list_shuffle

Defined in <`stapl/containers/graph/algorithms/graph_io.hpp`>

```c++
template<typename Graph,typename ShuffleMapperView>  
graph_view< Graph > read_edge_list_shuffle(ShuffleMapperView const & mapper,std::string filename,size_t blk_sz)
```

## Summary

Function to wrap around file-reader to support reading an edge-list and rename the vertices id when added to the graph view.


