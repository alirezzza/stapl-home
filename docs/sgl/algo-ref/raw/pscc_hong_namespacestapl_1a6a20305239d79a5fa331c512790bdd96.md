# pscc_hong

Defined in <`stapl/containers/graph/algorithms/pscc_kla_hong.hpp`>

```c++
template<typename GraphView>  
void pscc_hong(GraphView & g,size_t k,float degree_percentage)
```

## Summary



This function executes Hong's DCSC algorithm on the graph `g`. The algorithm works by selecting a pivot, traversing from this pivot, breaking the graph into subpieces, performing a special trim, separating weakly connected components, and then running DCSC (pscc_single) on the pieces. 
#### Parameters
* *g*: The graph_view over the input graph. 


* *k*: The maximum amount of asynchrony for kla. 


* *degree_percentage*: The threshold for a vertex's degree after which it will be considered a hub (given as a percentage of total graph size).





For DCSC, it is very important the group to which each node belongs be maintained constantly; the algorithm cannot use more than one pivot at a time on any group. We use two different methods of distinguishing the groups, depending on which part of the algorithm we are currently executing.

* (simple) While the traversals are being executed, the groups are distinguished only by what pivot is operating on them; this is denoted by the member m_group in pscc_single_vertex_property.


* (complex) While the new pivots are being selected, the groups are distinguished by a (pivot that just operated on me, how that pivot can reach me) pair. This information is encapsulated in the pscc_single_mark_type struct.
