# mark_vertices

Defined in <`stapl/containers/graph/algorithms/connected_components_hierarchical.hpp`>

```c++
template<typename Graph,typename Map,typename CMap>  
void mark_vertices(Graph & g,Map & map,CMap & cmap,typename Graph::vertex_descriptor src,typename Graph::vertex_descriptor mark)
```

## Summary

Mark all vertices reachable from a given source vertex with a specific identifier in the given map.

#### Parameters
* *g*: A sequential graph to traverse 


* *map*: A map that will be filled based on reachability from the source 


* *cmap*: A color map used internally by the traversal 


* *src*: A vertex from which to start the traversal 


* *mark*: An ID with which to mark vertices.
