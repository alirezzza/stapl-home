# bad_rank_h

Defined in <`stapl/containers/graph/algorithms/bad_rank.hpp`>

```c++
template<class GView,class HView>  
size_t bad_rank_h(GView & graph,HView & h,size_t iterations,size_t num_bl,double damping)
```

## Summary

Parallel Level-Synchronized BadRank Algorithm using the hierarchical machine paradigm ([h_paradigm()](#namespacestapl_1ab2f2aaf9b13ecd411395eae6d992804b)).

Computes the BadRank values of the vertices of the input graph_view, storing the ranks of all vertices on their properties. 
#### Parameters
* *graph*: The graph_view over the input graph. 


* *h*: The hierarchical machine graph_view over the input graph. This is generated by calling [create_level_machine()](#namespacestapl_1a642f8180b57c54c687fdccf799ec05bb) on the input graph_view. 


* *iterations*: The number of BadRank iterations to perform. 


* *damping*: The damping factor for the algorithm. 





#### Returns
The number of iterations performed.
