# flatten_hierarchy

Defined in <`stapl/containers/graph/algorithms/create_hierarchy.hpp`>

```c++
template<typename GraphView>  
graph_view< graph< DIRECTED, NONMULTIEDGES, typename GraphView::vertex_property::property_type, bool > > flatten_hierarchy(std::vector< GraphView > const & hierarchy)
```

## Summary

Algorithm to flatten multiple levels of hierarchy into a single level.

The edges of the output graph represent parent-child relationship in the hierarchy. The leaf-vertices represent the vertices of level-0 of the hierarchy and non-leaf vertices represent supervertices in intermediate-levels of the hierarchy.


#### Parameters
* *hierarchy*: An std::vector of graph_view representing the hierarchy. Size of the input reflects the number of levels in the hierarchy, and hierarchy[i] represents the graph at the i-th level in the hierarchy, with hierarchy[0] being the lowest level in the hierarchy. 





#### Returns
A graph_view representing the flattened hierarchy. The view is over a DIRECTED, NONMULTIEDGES static graph whose vertices have the user vertex-property stored on them and edge-property is a bool indicating if it is an edge from a vertex in a higher-level to a vertex in a lower-level (true) or vice-versa (false). 


> Todo: The naive fill algorithm may not distribute the work most effectively, we may want to find a better algorithm to improve distribute vertices.
