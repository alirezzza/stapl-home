# cut_conductance

Defined in <`stapl/containers/graph/algorithms/cut_conductance.hpp`>

```c++
template<typename View,typename MemberMap,typename CrossMap>  
double cut_conductance(View const & vw,MemberMap const & member_map,CrossMap const & cross_map,typename MemberMap::value_type const & id)
```

## Summary

Compute the conductance of a cut defined to be the set of vertices for which member(v) = id.

#### Parameters
* *vw*: The input graph_view. 


* *member_map*: Input vertex property map specifying the cut ids of vertices. 


* *cross_map*: Temporary vertex property map to store the number of cross edges 


* *id*: The cut id for which to compute conductance.
