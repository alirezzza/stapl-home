# betweenness_centrality

Defined in <`stapl/containers/graph/algorithms/betweenness_centrality.hpp`>

```c++
template<typename GView>  
void betweenness_centrality(GView & gv,size_t num_sources,bool partial)
```

## Summary

Computes betweenness centrality value of unweighted, directed graph.

#### Parameters
* *gv*: View of graph on which centrality will be computed. 


* *num_sources*: Number of sources to process at a time, if num_sources equals zero then all sources are processed at the the same time. 


* *partial*: Indicating whether or not all vertices should be sources. If not, only num_sources traversals are considered 





> Todo: Explore activation of non-contiguous ranges of vertices.
