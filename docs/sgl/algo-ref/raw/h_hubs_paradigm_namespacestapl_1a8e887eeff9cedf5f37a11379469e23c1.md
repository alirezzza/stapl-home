# h_hubs_paradigm

Defined in <`stapl/containers/graph/algorithms/paradigms/h_hubs_paradigm.hpp`>

```c++
template<typename WF,typename UF,typename VPR,typename HView,typename HubsView,typename GView>  
size_t h_hubs_paradigm(WF const & uwf,UF const &,VPR const & vpr,HView & h,HubsView & hubs,GView & g)
```

## Summary

The Parallel Hierarchical Hubs Level-Synchronous (h-hubs) Paradigm.

Overloaded variant of [h_hubs_paradigm()](#namespacestapl_1a8d958fad783ec030aa95c560587efcc7) with an empty post-execute.
