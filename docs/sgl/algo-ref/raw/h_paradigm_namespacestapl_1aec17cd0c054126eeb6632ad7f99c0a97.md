# h_paradigm

Defined in <`stapl/containers/graph/algorithms/paradigms/h_paradigm.hpp`>

```c++
template<typename WF,typename UF,typename HView,typename GView>  
size_t h_paradigm(WF const & uwf,UF const &,HView & h,GView & g)
```

## Summary

The Parallel Level-Synchronous (H) Paradigm.

Overloaded variant of [h_paradigm()](#namespacestapl_1ab2f2aaf9b13ecd411395eae6d992804b) with an empty post-execute.
