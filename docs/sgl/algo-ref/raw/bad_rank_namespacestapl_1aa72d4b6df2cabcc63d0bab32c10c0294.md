# bad_rank

Defined in <`stapl/containers/graph/algorithms/bad_rank.hpp`>

```c++
template<typename GView>  
size_t bad_rank(GView & graph,size_t iterations,size_t num_bl,double damping)
```

## Summary

Parallel Level-Synchronized BadRank Algorithm.

"BadRank is a method for detecting spam web sites, based on the premise that a page is spam if it points to another spam page; i.e., the BadRank score of a page is the weighted sum of the BadRank scores of the pages that it links to." [^1] Computes the BadRank values of the vertices of the input graph_view, storing the ranks of all vertices on their properties. A vertex is marked as spam by setting its "blacklisted" property.


#### Parameters
* *graph*: The graph_view over the input graph. 


* *iterations*: The number of BadRank iterations to perform. 


* *num_bl*: The number of blacklisted vertices in the graph. 


* *damping*: The damping factor for the algorithm.





**See also**: [properties::bad_rank_property](#classstapl_1_1properties_1_1bad__rank__property)

#### Example
```cpp
  using property_type = stapl::properties::bad_rank_property;
  using graph_type = stapl::multidigraph<property_type>;

  auto g = stapl::read_edge_list<graph_type>(argv[1]);
  std::size_t num_iters = 20;

  // Vertex 1 is a known spam site
  g[1].property().set_blacklisted(true);

  stapl::bad_rank(g, num_iters, 1, 0.8);

  std::cout << "Vertex 0 has rank " << g[0].property().rank();
```
 [^1] T. G. Kolda and M. J. Procopio. “Generalized BadRank with Graduated Trust.” Sandia National Laboratories Technical Report #2009-6670 (2009).
