# page_rank

Defined in <`stapl/containers/graph/algorithms/page_rank.hpp`>

```c++
template<typename GView>  
size_t page_rank(GView & graph,size_t iterations,double damping)
```

## Summary

Parallel Level-Synchronized PageRank Algorithm.

Performs a [PageRank]([https://en.wikipedia.org/wiki/PageRank](https://en.wikipedia.org/wiki/PageRank)) on the input graph_view, storing the ranks of all vertices on their properties. 
#### Parameters
* *graph*: The graph_view over the input graph. 


* *iterations*: The number of PageRank iterations to perform. 


* *damping*: The damping factor for the algorithm. 





#### Returns
The number of iterations performed.


**See also**: [properties::page_rank_property](#classstapl_1_1properties_1_1page__rank__property)

#### Example
```cpp
  using property_type = stapl::properties::page_rank_property;
  using graph_type = stapl::multidigraph<property_type>;

  auto g = stapl::read_edge_list<graph_type>(argv[1]);
  std::size_t num_iters = 20;

  stapl::page_rank(g, num_iters, 0.8);

  std::cout << "Vertex 0 has rank " << g[0].property().rank();
```
