# sssp

Defined in <`stapl/containers/graph/algorithms/sssp.hpp`>

```c++
template<class GView>  
size_t sssp(GView & g,typename GView::vertex_descriptor const & source,size_t k)
```

## Summary

A Parallel Single-Source Shortest Paths (SSSP) algorithm.

Performs a single-source shortest path query on the input graph_view, storing the shortest path distance and parent on each reachable vertex. All vertices will be initialized with their distance as infinity (MAX) and their active state as false, except the source-vertex, which will have its distance set to zero (0) and active state set to true. Parents of each vertex will be initialized to the vertex's descriptor. 
#### Parameters
* *g*: The graph_view over the input graph. 


* *source*: The descriptor of the source vertex for this traversal. 


* *k*: The maximum amount of asynchrony allowed in each phase. 0 <= k <= inf. k == 0 implies level-synchronous SSSP. k >= D implies fully asynchronous SSSP (D is diameter of graph). 





#### Returns
The number of iterations performed by the paradigm. 

If the graph is unweighted, a breadth-first search may be a more suitable computation.


**See also**: [properties::sssp_property](#classstapl_1_1properties_1_1sssp__property)

#### Example
```cpp
  using property_type = stapl::properties::sssp_property;
  using graph_type = stapl::multidigraph<property_type, int>;

  auto g = stapl::read_weighted_edge_list<graph_type>(argv[1]);

  stapl::sssp(g, 0, 10);

  int dist = g[10].property().distance();

  std::cout << "vertex 10 is distance " << dist << " away from vertex 0";
```
