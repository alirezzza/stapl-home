# get_graph_colors

Defined in <`stapl/containers/graph/algorithms/graph_coloring.hpp`>

```c++
template<typename GView,typename ColorMap>  
domset1D< size_t > get_graph_colors(GView const & graph,ColorMap & color_map)
```

## Summary

Extracts the colors from the input graph, given the color map.

#### Parameters
* *graph*: The graph_view over the input graph. 


* *color_map*: The input vertex property map where the color information is stored. [vertex->color (int)]. Must have been previously populated by calling [color_graph()](#namespacestapl_1a7ef86fe8b7e7af4d903f22f940d17a1b). 





#### Returns
The domain of colors in the graph.
