# pscc_schudy

Defined in <`stapl/containers/graph/algorithms/pscc_schudy.hpp`>

```c++
template<typename GraphView>  
void pscc_schudy(GraphView g)
```

## Summary

This is an implementation of Schudy's MultiPivot SCC algorithm,.

This algorithm executes Schudy's SCC algorithm on the input graph. It first finds a pivot such that the pivot and some other nodes reach at least half of the graph. It then tests reachability from the pivot and divides the graph into the unreached and reached node sets. This algorithm is O(log n) times slower than DCSCMulti (pscc.h), and so should not be used other than for comparison. 


#### Parameters
* *g*: The input graph_view.
