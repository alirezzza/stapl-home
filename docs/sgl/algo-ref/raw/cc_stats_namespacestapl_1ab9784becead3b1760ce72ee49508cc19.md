# cc_stats

Defined in <`stapl/containers/graph/algorithms/connected_components.hpp`>

```c++
template<typename GView,typename VertexDescriptor,typename ResView>  
void cc_stats(GView & g,VertexDescriptor CCid,ResView & output)
```

## Summary

Algorithm to compute all vertices belonging to the specified connected-component.

Given a Graph, and Connected-Component IDs for each vertex, this returns all vertices belonging to the connected-component specified by CCid. 
#### Parameters
* *g*: The graph_view over the input graph. property_type on the vertex needs to have methods: vertex_descriptor cc(void)  for getting the connected-component id. cc(vertex_descriptor)  for setting the connected-component id. The connected-component for each vertex must be available, or be filled-in by calling connected_components(g) algorithm before calling cc_stats. 


* *CCid*: The connected-component ID of the desired connected-component. 


* *output*: The output view used to store and return vertices belonging to the specified connected-component. Can be a view over stapl::array<std::vector<VD> > or any 1-d view of std::vectors<VD>. Each location stores the local vertices belonging to the specified CC.
