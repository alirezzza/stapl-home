# random_walk

Defined in <`stapl/containers/graph/algorithms/random_walk.hpp`>

```c++
template<typename GView>  
array_view< stapl::array< typename GView::vertex_descriptor > > random_walk(GView & graph,typename GView::vertex_descriptor const & source,size_t const & path_length,size_t const & k)
```

## Summary

Perform a parallel random walk from a given source vertex of a specified length.

The algorithm starts at the given vertex and randomly chooses one of its out edges as the next edge in the walk. The algorithm moves to the chosen edge's target vertex and repeats the process until the walk has reached the desired length. If a sink is reached before the desired length is reached, the algorithm returns the path found up to that point.


#### Parameters
* *graph*: The graph_view over the input graph. 


* *path_length*: The desired path length. 


* *k*: The maximum amount of asynchrony allowed in each phase. 0 <= k <= inf. k == 0 implies level-synchronous random walk. k >= D implies fully asynchronous random walk (D is diameter of graph). 





#### Returns
The vertices in the resulting path. A value of infinity denotes an invalid value, implying the path was shortened to that point.


**See also**: [properties::rw_property](#classstapl_1_1properties_1_1rw__property)

#### Example
```cpp
  using property_type = stapl::properties::rw_property;
  using graph_type = stapl::multidigraph<property_type>;

  auto g = stapl::read_edge_list<graph_type>(argv[1]);
  std::size_t num_hops = 4;

  auto path = stapl::random_walk(g, 0, num_hops);

  std::cout << "Random walk from vertex 0:" << std::endl;
  for (auto const& v : path)
    std::cout << v << " ";
```
