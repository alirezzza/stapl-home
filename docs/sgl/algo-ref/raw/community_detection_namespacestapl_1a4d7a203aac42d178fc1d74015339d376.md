# community_detection

Defined in <`stapl/containers/graph/algorithms/community_detection.hpp`>

```c++
template<typename GView>  
size_t community_detection(GView & graph,size_t max_iter)
```

## Summary

Parallel Level-Synchronized Community Detection Algorithm based on modularity maximization.

Performs a variant of label-propagation on the input graph_view, to find the most frequent label in each vertex's neighborhood, which is set as its community on its property. 
#### Parameters
* *graph*: The graph_view over the input graph. 


* *max_iter*: The maximum number of iterations allowed to execute. 





#### Returns
The number of iterations performed.
