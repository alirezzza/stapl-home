# write_dot

Defined in <`stapl/containers/graph/algorithms/graph_io.hpp`>

```c++
template<typename GraphVw>  
void write_dot(GraphVw & g,std::string filename)
```

## Summary

Function to output a DOT graph, without properties.

#### Parameters
* *g*: The input graph_view to be printed. 


* *filename*: The name of the file where the graph will be printed.
