# create_level_partial_info

Defined in <`stapl/containers/graph/algorithms/create_level_partial_info.hpp`>

```c++
template<typename GraphView,typename PropMap,typename VertexPReducer,typename EdgePReducer>  
GraphView create_level_partial_info(GraphView & gvw,PropMap & partial_vertex_group_map,VertexPReducer vpr,EdgePReducer epr,size_t max_msg_sz,bool self_edges)
```

## Summary

Algorithm to create an entire level of hierarchy at once, based on partial vertex-matching information.

Creates a new level of hierarchy based on the input graph_view and the provided vertex-grouping. Supervertices and superedges in the resulting output graph store descriptors of their children in the input. Properties of supervertices and superedges are reductions of the properties of their children through the user provided reducers.

#### Parameters
* *gvw*: The input graph_view over which the new level is to be created. The underlying graph container must be DIRECTED, NONMULTIEDGES, and store the custom [super_vertex_property](#structstapl_1_1super__vertex__property) and [super_edge_property](#structstapl_1_1super__edge__property) on their vertices and edges, respectively. 


* *partial_vertex_group_map*: Vertex property map identifying the "group" of each vertex in the graph. The leader-vertex of each group is the vertex whose group ID is the same as its descriptor. There must be exactly one leader vertex in each group. Non-leader vertices may choose to "collapse" with other leader or non-leader vertices. The final grouping with which to create the supervertex is calculated by running a pointer-jumping (or similar) algorithm . [vertex->vertex_descriptor] 


* *vpr*: The vertex property reducer for reducing child-vertex properties to form the supervertex property. 


* *epr*: The edge property reducer for reducing child-edge properties to form the superedge property. 


* *max_msg_sz*: The number of requests to aggregate for the aggregator. 


* *self_edges*: Indicates whether self-edges are allowed on the output graph or not. If the supergraph has self-edges, they will represent the internal edges between the supervertex's child vertices. 





#### Returns
A graph_view over the output supergraph. The type of this view is the same as the input graph_view.
