# kla_paradigm

Defined in <`stapl/containers/graph/algorithms/paradigms/kla_paradigm.hpp`>

```c++
template<typename WF,typename UF,typename Scheduler,typename GView,typename Predicate>  
size_t kla_paradigm(WF const & uwf,UF const &,Scheduler const & scheduler,GView & g,size_t k,[kla_params](#structstapl_1_1kla__params)< GView, Predicate > const & params)
```

## Summary

The Parallel k-Level-Asynchronous (KLA) Paradigm.

Implements the KLA paradigm, which iteratively executes KLA-Supersteps (KLA-SS). Each KLA-SS can perform asynchronous visits up to 'k' levels deep. Any communication generated within a KLA-SS is guaranteed to have finished before the KLA-SS finishes. The user provides a vertex-operator to express the computation to be performed on each vertex, and a neighbor-operator that will be applied to each neighbor that is visited. The vertex-operator is passed in a vertex and a visit object. To visit a neighboring vertex, the vertex-operator must call visit(neighbor, neighbor-operator()). The vertex-operator must return true if the vertex was active (i.e. its value was updated), or false otherwise. The neighbor-operator is passed in the target vertex. Neighbor-operators may carry state, but must be immutable. They should return true if the visit was successful (i.e. the target vertex will be activated after this visit), or false otherwise. Users may also provide additional functions to be executed before and after each KLA-SS. 
#### Parameters
* *WF*: The type of the user provided vertex-operator expressing computation to be performed over each vertex. 


* *UF*: The type of the user provided neighbor-operator expressing computation to be performed over neighboring vertices. 





#### Parameters
* *uwf*: Functor that implements the operation to be performed on each vertex 


* *post_execute*: Optional functor that will be executed on the graph_view at the end of each KLA-SS. This will be invoked with the input graph_view and the current KLA-SS ID (the ID of the KLA-SS that just finished). 


* *g*: The graph_view over the input graph. 


* *k*: The maximum amount of asynchrony allowed in each phase. 0 <= k <= inf. k == 0 implies level-synchronous KLA. k >= D performed implies fully asynchronous KLA (D is the number of iterations by the level-synchronous variant of the algorithm). 





#### Returns
The number of iterations performed by the paradigm.
