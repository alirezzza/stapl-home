# create_level

Defined in <`stapl/containers/graph/algorithms/create_level.hpp`>

```c++
template<typename GraphView,typename PropMap,typename VertexPReducer,typename EdgePReducer>  
GraphView create_level(GraphView & gvw,std::pair< bool, PropMap > const & vertex_grouping,VertexPReducer vpr,EdgePReducer epr,size_t max_msg_sz,bool self_edges,bool sort_edges)
```

## Summary

Algorithm to create an entire level of hierarchy at once.

Creates a new level of hierarchy based on the input graph_view and the provided vertex-grouping. Supervertices and superedges in the resulting output graph store descriptors of their children in the input. Properties of supervertices and superedges are reductions of the properties of their children through the user provided reducers.

#### Parameters
* *gvw*: The input graph_view over which the new level is to be created. The underlying graph container must be DIRECTED, NONMULTIEDGES, and store the custom [super_vertex_property](#structstapl_1_1super__vertex__property) and [super_edge_property](#structstapl_1_1super__edge__property) on their vertices and edges, respectively. 


* *vertex_grouping*: An std::pair of a bool and a vertex property map. The bool indicates whether the matching information is complete (true) or partial (false). The vertex property map identifies the "group" of each vertex in the graph. The leader-vertex of each group is the vertex whose group ID is the same as its descriptor. There must be exactly one leader vertex in each group. All non-leader vertices must either chose indicate one of the leader-vertices as their group, with whom they would like to "collapse" to form the supervertex (complete grouping), OR the non-leader vertices may choose to "collapse" with other leader or non-leader vertices (partial grouping). In case of partial grouping, the final grouping with which to create the supervertex is calculated by running a pointer-jumping (or similar) algorithm . [vertex->vertex_descriptor] 


* *vpr*: The vertex property reducer for reducing child-vertex properties to form the supervertex property. 


* *epr*: The edge property reducer for reducing child-edge properties to form the superedge property. 


* *max_msg_sz*: The number of requests to aggregate for the aggregator. 


* *self_edges*: Indicates whether self-edges are allowed on the output graph or not. If the supergraph has self-edges, they will represent the internal edges between the supervertex's child vertices. 


* *sort_edges*: Indicates if the adjacent-edges for each supervertex in the output supergraph will be sorted based on their target-descriptors. 





#### Returns
A graph_view over the output supergraph. The type of this view is the same as the input graph_view.
