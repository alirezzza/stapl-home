# read_dimacs

Defined in <`stapl/containers/graph/algorithms/graph_io.hpp`>

```c++
template<typename Graph>  
graph_view< Graph > read_dimacs(std::string filename,size_t blk_sz)
```

## Summary

Function to wrap around file-reader to support reading a DIMACS graph.


