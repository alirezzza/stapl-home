# create_level

Defined in <`stapl/containers/graph/algorithms/hierarchical_view.hpp`>

```c++
template<typename VertexPartitioner,typename EdgeFunctor,typename PG,typename Dom,typename MapFunc,typename Derived>  
[hierarchical_graph_view](#classstapl_1_1hierarchical__graph__view)< dynamic_graph< DIRECTED, MULTIEDGES, [hierarchical_graph_view](#classstapl_1_1hierarchical__graph__view)< PG, typename [extract_domain_type](#structstapl_1_1extract__domain__type)< PG, VertexPartitioner >::type >, typename EdgeFunctor::value_type >> create_level(const graph_view< PG, Dom, MapFunc, Derived > & graph,const VertexPartitioner & partitioner,const EdgeFunctor & ef)
```

## Summary

Algorithm to create a level in the hierarchy of graphs, using the user-provided vertex partitioner and edge-functor.

#### Parameters
* *graph*: A graph_view over the input graph. 


* *partitioner*: The vertex paritioner which takes an input graph and returns the partition of vertices on next level, along with descriptors corresponding to them. 


* *ef*: The edge functor which takes graph of the next level, and adds the required edges. 





#### Returns
A [hierarchical_graph_view](#classstapl_1_1hierarchical__graph__view) over the graph of the next-level.


> Todo: Directed and Multiedges should be taken from the base graph
