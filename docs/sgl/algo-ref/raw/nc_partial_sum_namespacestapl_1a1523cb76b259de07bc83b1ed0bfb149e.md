# nc_partial_sum

Defined in <`stapl/containers/graph/algorithms/nc_map.hpp`>

```c++
template<typename View0,typename View1>  
void nc_partial_sum(View0 const & view0,View1 const & view1,bool shift)
```

## Summary

partial_sum() variant that does not coarsen the input views if size of `view0` is less than the number of locations.

This is needed to make partial-sum work with views with sizes less than the number of locations until the metadata reported is corrected to exclude empty base containers.
