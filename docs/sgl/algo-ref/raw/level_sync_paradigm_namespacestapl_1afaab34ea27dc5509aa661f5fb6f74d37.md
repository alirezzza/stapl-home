# level_sync_paradigm

Defined in <`stapl/containers/graph/algorithms/paradigms/level_sync_paradigm.hpp`>

```c++
template<typename WF,typename UF,typename GView>  
size_t level_sync_paradigm(WF const & uwf,UF const &,GView & g)
```

## Summary

The Parallel Level-Synchronous (Level_Sync) Paradigm.

Overloaded variant of [level_sync_paradigm](#namespacestapl_1a095a21ed39828b6ddcf566cc337745bb) with an empty post-execute.
