# pseudo_diameter

Defined in <`stapl/containers/graph/algorithms/pseudo_diameter.hpp`>

```c++
template<typename GView>  
size_t pseudo_diameter(GView & graph,size_t k)
```

## Summary

Parallel Pseudo-Diameter Algorithm.

Calculates the pseudo-diameter of a graph, using a chain of BFS traversals. 
#### Parameters
* *graph*: The graph_view over the input graph. 


* *k*: The level of asynchrony in the algorithm. 





#### Returns
The pseudo-diameter of the graph.
