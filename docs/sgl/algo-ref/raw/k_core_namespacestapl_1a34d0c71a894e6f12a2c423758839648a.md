# k_core

Defined in <`stapl/containers/graph/algorithms/k_core.hpp`>

```c++
template<typename GView,typename Comp>  
size_t k_core(GView & graph,int core_sz,size_t k,Comp comp)
```

## Summary

A Parallel k-core algorithm.

Iteratively marks all vertices not satisfying the specified comparison of the vertex's out-degree with the provided k parameter, as 'deleted', i.e. the vertex property is set to -1. 
#### Parameters
* *graph*: The graph_view over the input graph. 


* *core_sz*: The parameter to control which vertices are deleted. 


* *k*: The maximum amount of asynchrony allowed in each phase. 0 <= k <= inf. k == 0 implies level-synchronous paradigm. k >= D implies fully asynchronous paradigm (D is diameter of graph). 


* *comp*: The comparator to compare the vertex's out-degree with k. 





#### Returns
The number of iterations performed by the paradigm. Note this is different from the dynamic version which returns the number of vertices deleted.
