# k_core_dynamic

Defined in <`stapl/containers/graph/algorithms/k_core_dynamic.hpp`>

```c++
template<typename GView,typename Comp>  
size_t k_core_dynamic(GView & graph,size_t k,Comp comp)
```

## Summary

A Parallel k-core algorithm. Iteratively deletes all vertices not satisfying the specified comparison of the vertex's out-degree with the provided k parameter.

#### Parameters
* *graph*: The graph_view over the input graph. 


* *k*: The parameter to control which vertices are deleted. 


* *comp*: The comparator to compare the vertex's out-degree with k.
