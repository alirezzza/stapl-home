# rebalance_global

Defined in <`stapl/containers/graph/algorithms/rebalance_global.hpp`>

```c++
template<typename View>  
void rebalance_global(View & vw)
```

## Summary

Parallel global rebalancer based on number of vertices.

Balances the graph to even-out the number of vertices on each location. Does one-shot global rebalancing. 
#### Parameters
* *vw*: The graph_view over the input graph. 




Invalidates the input graph_view.
