# preflow_push

Defined in <`stapl/containers/graph/algorithms/preflowpush.hpp`>

```c++
template<typename GraphView>  
stapl::tuple< double, double, double, double > preflow_push(GraphView & gView,size_t source,size_t sink,int async)
```

## Summary

Initializes the properties of vertices and edges for the preflow push algorithm, and calculates the max flow in the graph.

#### Parameters
* *gView*: View of the graph. 


* *source*: Source of the flow network. 


* *sink*: Sink of the flow network. 


* *async*: Level of asynchrony to use for the graph paradigm.
