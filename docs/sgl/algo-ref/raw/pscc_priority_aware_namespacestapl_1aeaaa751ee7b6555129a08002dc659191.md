# pscc_priority_aware

Defined in <`stapl/containers/graph/algorithms/pscc_kla_priority_aware.hpp`>

```c++
template<typename GraphView>  
void pscc_priority_aware(GraphView & g,size_t k,float degree_percentage)
```

## Summary



This function executes the SCC2 algorithm on the graph `g`. The algorithm works by traversing from all nodes breaking the graph into subpieces, and repeating this operation until every node is allocated to an SCC. 
#### Parameters
* *g*: The graph_view over the input graph. 


* *k*: The maximum amount of asynchrony for kla. 


* *degree_percentage*: The threshold for a vertex's degree after which it will be considered a hub (given as a percentage of total graph size).
