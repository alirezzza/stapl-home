# page_rank_kla

Defined in <`stapl/containers/graph/algorithms/page_rank_kla.hpp`>

```c++
template<typename GView>  
size_t page_rank_kla(GView & g,size_t num_iter,size_t k)
```

## Summary

Parallel k-level-asynchronous PageRank Algorithm.

Performs a PageRank on the input graph_view, storing the ranks of all vertices on their properties. 
#### Parameters
* *graph*: The graph_view over the input graph. 


* *num_iter*: The number of PageRank iterations to perform. 


* *k*: The level of asynchrony. 





#### Returns
The number of iterations performed.
