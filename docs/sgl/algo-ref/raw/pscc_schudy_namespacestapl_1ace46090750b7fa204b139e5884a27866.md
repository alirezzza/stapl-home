# pscc_schudy

Defined in <`stapl/containers/graph/algorithms/pscc_kla_schudy.hpp`>

```c++
template<typename GraphView>  
int pscc_schudy(GraphView & g,size_t k,float degree_percentage)
```

## Summary

This is an implementation of Schudy's MultiPivot SCC algorithm.

This algorithm executes a variant of Schudy's SCC algorithm on the input graph. It first finds a pivot such that the pivot and some other nodes reach at least half of the graph. It then tests reachability from the pivot and divides the graph into the unreached and reached node sets. This algorithm is O(log n) times slower than SCCMulti (pscc.hpp), and so should not be used other than for comparison. 


#### Parameters
* *g*: The input graph_view. 


* *k*: The maximum amount of asynchrony for kla. 


* *degree_percentage*: The threshold for a vertex's degree after which it will be considered a hub (given as a percentage of total graph size).
