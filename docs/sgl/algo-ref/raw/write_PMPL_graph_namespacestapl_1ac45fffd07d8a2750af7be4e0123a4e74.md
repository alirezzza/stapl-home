# write_PMPL_graph

Defined in <`stapl/containers/graph/algorithms/graph_io.hpp`>

```c++
template<typename GraphVw>  
void write_PMPL_graph(GraphVw & g,std::string filename)
```

## Summary

Function to output the graph in PMPL format.

#### Parameters
* *g*: The input graph_view to be printed. 


* *filename*: The name of the file where the graph will be printed.
