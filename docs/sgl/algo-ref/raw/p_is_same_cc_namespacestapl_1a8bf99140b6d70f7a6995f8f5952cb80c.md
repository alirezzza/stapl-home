# p_is_same_cc

Defined in <`stapl/containers/graph/algorithms/connected_components_hierarchical.hpp`>

```c++
template<typename GView,typename CCMap,typename VertexDescriptor>  
bool p_is_same_cc(GView & g,CCMap & map,VertexDescriptor v1,VertexDescriptor v2)
```

## Summary

Given a graph and a connected component map of vertices, determine if two vertices are in the same connected component.

This has the assumption that a map is first populated using the [p_cc_stats()](#namespacestapl_1a663498c1bd4b363b3c8c628058b93989) algorithm.


#### Parameters
* *g*: A View of the graph 


* *map*: A property map of connected component IDs 


* *v1*: *v2*: Two vertices to query. 





#### Returns
True if the two input vertices are in the same connected component.
