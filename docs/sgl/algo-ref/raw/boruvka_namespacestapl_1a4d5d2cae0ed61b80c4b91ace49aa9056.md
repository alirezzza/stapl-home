# boruvka

Defined in <`stapl/containers/graph/algorithms/boruvka.hpp`>

```c++
template<typename View>  
View boruvka(View & g)
```

## Summary

This method implements Boruvka's Minimum-Spanning Tree (MST) algorithm.

#### Parameters
* *g*: the graph view of the undirected input graph. 





#### Returns
The graph view storing the resulting minimum spanning tree of `g`.
