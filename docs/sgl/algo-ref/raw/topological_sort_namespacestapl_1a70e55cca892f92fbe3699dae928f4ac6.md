# topological_sort

Defined in <`stapl/containers/graph/algorithms/topological_sort.hpp`>

```c++
template<typename GraphView>  
void topological_sort(GraphView & gvw,size_t k)
```

## Summary

A Parallel Topological Sort algorithm.

The input graph must be a Directed Acyclic Graph (DAG). Performs a topological ordering on the input graph_view, storing the topological ordering in the vertex's rank property. This algorithm iteratively finds source-vertices and assigns the next level in the topological ordering to neighboring vertices, while decrementing their in-degrees. When the in-degree reaches zero (0), the vertex becomes a source vertex. 
#### Parameters
* *gvw*: The graph_view over the input graph. 


* *k*: The maximum amount of asynchrony allowed in each phase. 0 <= k <= inf. k == 0 implies level-synchronous topological sort. k >= D implies fully asynchronous topological sort (D is diameter of graph).





> Todo: Add another function that will produce an array_view whose elements represent a valid topological sort using the topological levels computed. This is needed by PDT and other applications that need a topological order that they can use to enforce ordering.
