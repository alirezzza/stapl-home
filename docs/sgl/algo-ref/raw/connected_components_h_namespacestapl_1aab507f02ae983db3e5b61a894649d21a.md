# connected_components_h

Defined in <`stapl/containers/graph/algorithms/connected_components.hpp`>

```c++
template<class GView,class HView,class HubsView>  
size_t connected_components_h(GView & g,HView & h,HubsView & hubs)
```

## Summary

A Parallel Connected Components (CC) Algorithm using the hierarchical machine and hubs paradigm ([h_hubs_paradigm()](#namespacestapl_1a8d958fad783ec030aa95c560587efcc7)).

Marks all vertices belonging to the same CC with the same connected-component ID. 
#### Parameters
* *g*: The graph_view over the input graph. 


* *h*: The hierarchical machine graph_view over the input graph. This is generated by calling [create_level_machine()](#namespacestapl_1a642f8180b57c54c687fdccf799ec05bb) on the input graph_view. 


* *hubs*: The hierarchical hubs graph_view over the input graph, which creates a hierarchical view over all the high-degree vertices (hubs) of the input graph. This is generated by calling [create_level_hubs()](#namespacestapl_1a71fbd54c2a39438d6615da4bbb7d8c11) on the input graph_view. property_type on the vertex needs to have methods: vertex_descriptor cc(void)  for getting the connected-component id. cc(vertex_descriptor)  for setting the connected-component id. bool active(void)  for checking if vertices are active. void active(bool)  for marking vertices as active. 





#### Returns
The number of iterations performed by the algorithm.
