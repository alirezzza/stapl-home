# nc_map_reduce

Defined in <`stapl/containers/graph/algorithms/nc_map.hpp`>

```c++
template<typename Functor1,typename Functor2,typename View0,typename... View>  
Functor2::result_type nc_map_reduce(Functor1 const & func1,Functor2 const & func2,View0 const & view0,View const &... view)
```

## Summary

map_reduce() variant that does not coarsen the input views if size of `view0` is less than the number of locations.

This is needed to make map-reduce work with views with sizes less than the number of locations until the metadata reported is corrected to exclude empty base containers.
