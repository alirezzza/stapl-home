# nc_accumulate

Defined in <`stapl/containers/graph/algorithms/nc_map.hpp`>

```c++
template<typename View>  
View::value_type nc_accumulate(View const & view,typename View::value_type init)
```

## Summary

accumulate() variant that does not coarsen the input views if size of `view` is less than the number of locations.

This is needed to make accumulate work with views with sizes less than the number of locations until the metadata reported is corrected to exclude empty base containers.
