# closeness_centrality

Defined in <`stapl/containers/graph/algorithms/closeness_centrality.hpp`>

```c++
template<class GView,class HView,class HubsView>  
void closeness_centrality(GView & g,HView & h,HubsView & hubs)
```

## Summary

Parallel Closeness Centrality Algorithm using the hierarchical and hubs breadth-first search ([breadth_first_search_h()](#namespacestapl_1ada28d2fc82b4150e7bbbb357c886315b)).

#### Parameters
* *g*: The graph_view over the input graph. 


* *h*: The hierarchical machine graph_view over the input graph. This is generated by calling [create_level_machine()](#namespacestapl_1a642f8180b57c54c687fdccf799ec05bb) on the input graph_view. 


* *hubs*: The hierarchical hubs graph_view over the input graph, which creates a hierarchical view over all the high-degree vertices (hubs) of the input graph. This is generated by calling [create_level_hubs()](#namespacestapl_1a71fbd54c2a39438d6615da4bbb7d8c11) on the input graph_view.
