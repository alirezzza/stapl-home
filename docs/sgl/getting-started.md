Building
===
Compilers supported are:
  - GCC 4.8.2 - 4.9.2
  - Clang 3.5 - 3.7 (using libstdc++ for STL)
  - Intel C++ 16 (using gcc version 4.8.3 compatability)

The version of Boost required is 1.56.

Compiling the STAPL library
-----
Compiling the STAPL library requires setting the platform and stl variables.
For example, on vulcan the library can be compiled by running

```
$ gmake platform=BGQ_gcc stl=./tools/libstdc++/4.8.4
```

The result of this command will be `lib/libstapl.a` and `lib/libstapl_rt.a`.


Compiling STAPL programs
-----
When compiling a STAPL program there are several flags required.

- `-D_STAPL`: used in some components that are used in other Parasol projects to
  detect a STAPL application is being compiled rather than a
  sequential application

- `-I $(STAPL_ROOT)/$(stl)`: required so that versions of the STL and Boost
- `-I $(STAPL_ROOT)/tools`: headers with define_type methods (used in
  serialization) are available

- `-I $(STAPL_ROOT)/`: required so the stapl headers in stapl/ are
  available.  Include statements are of the form
  `#include <stapl/array.hpp>`

- `-I $(BOOST_ROOT)/include`: STAPL requires Boost 1.56

- `-DBOOST_RESULT_OF_USE_TR1_WITH_DECLTYPE_FALLBACK`: required by mechanism that
  detects work function return types

- `-std=c++11`: STAPL requires c++11 support

- `-L$(STAPL_ROOT)/lib -lstapl -lrt`: path to libstapl.a and the libraries
  required

- `-L$(BOOST_ROOT)/lib -lboost_serialization -lboost_thread -lboost_system`:
  Boost libraries required by STAPL runtime. These options must come after
  `-lstapl` in the compile line.


There are also several optional compile flags:

- `-DSTAPL_NDEBUG` -- disable runtime assertions, which can perform expensive
  computations as part of their check

- `-DSTAPL_RUNTIME_USE_OMP -fopenmp` -- explicitly specify runtime used for
  mixed-mode


To see a full command line cd to examples/statistics and run `gmake stats1`. Note that in addition to the Boost libraries mentioned above, this test also requires the Boost program options library to link (i.e., `-lboost_program_options`).

Additional compiler options can be specified as part of the make command through the USER_CXXFLAGS and USER_LIB variables if the STAPL Makefiles are used.


Running STAPL programs
=====

STAPL programs execute in mixed-mode at all times.  To run MPI-only with a single thread per process you set `STAPL_NUM_THREADS=1` in your environment.  If `STAPL_NUM_THREADS` is not set, execution will terminate.
