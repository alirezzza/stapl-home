# Implicit

When building a graph, the partition is part of the type of the graph. If a partition
type is not provided by the user, a sensible default will be automatically used.
For example, if a user uses `stapl::digraph<int>`, the partition is not specified
and a block distribution will be used.

## Block Partition

A block partition is a simple partitioning scheme wherein the vertices are evenly
distributed across the $$p$$ locations based on their numeric vertex descriptor.
For $$n$$ vertices, each location will contain approximately $$\frac{n}{p}$$
vertices.

In the default implicit block partition, the graph's descriptor space will be partitioned
across $$p$$ partitions, where the first $$0 \dots \frac{n}{p}$$ vertices will be
placed on location 0, the next $$\frac{n}{p} \dots 2 \times \frac{n}{p}$$ descriptors
will be on location 1 and so on.

For example, consider the following example graph ($$n=20$$) whose vertex descriptors
span from 0 to 19:

![Original](graph_partition1.png)


If we partition the graph on four locations, it would look like this:

![Block](graph_partition2.png)
