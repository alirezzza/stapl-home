# make_random_neighborhood

Defined in <`stapl/containers/graph/generators/random_neighborhood.hpp`>

```c++
template<typename GraphView>  
GraphView make_random_neighborhood(size_t n,size_t ef,size_t k,bool bidirectional)
```

## Summary

Generates a random graph with N vertices.

Each vertex can connect to others in its neighborhood (+/- k from a vertex's id defines the neighborhood). `k=1` forms a circular list, while `k=N` forms an Erdos-Renyi random network. Each vertex connects to `ef` neighbors on average, this is not the guaranteed number of edges for a particular vertex. The diameter of the generated graph decreases as `k` increases.

The returned view owns its underlying container.


#### Parameters
* *n*: Number of vertices in the generated graph. 


* *ef*: Average number of outgoing edges per vertex. 


* *k*: Size of the neighborhood to which each vertex can connect. 


* *bidirectional*: True to add back-edges in a directed graph, false for forward edges only. 





#### Returns
A view over the generated graph.

#### Example
```cpp
  using view_type = stapl::graph_view<stapl::multidigraph<int>>;

  auto v = stapl::generators::make_random_neighborhood<view_type>(
    128, 8, 4, true
  );
```
