# make_mesh

Defined in <`stapl/containers/graph/generators/mesh.hpp`>

```c++
template<typename GraphView>  
GraphView make_mesh(size_t nx,size_t ny,bool bidirectional)
```

## Summary

Generates an x-by-y mesh.

The generated mesh will contain x vertices in the horizontal direction and y vertices in the vertical direction. This is distributed n/p in the y-direction.

The returned view owns its underlying container. 
#### Parameters
* *nx*: Size of the x-dimension of the mesh. 


* *ny*: Size of the y-dimension of the mesh. 


* *bidirectional*: True to add back-edges in a directed graph, false for forward edges only. 





#### Returns
A view over the generated graph.

#### Example
```cpp
  using view_type = stapl::graph_view<stapl::multidigraph<int>>;

  auto v = stapl::generators::make_mesh<view_type>(256, 256, true);
```
