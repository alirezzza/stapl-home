# make_binary_tree

Defined in <`stapl/containers/graph/generators/binary_tree.hpp`>

```c++
template<typename GraphView>  
GraphView make_binary_tree(size_t n,bool bidirectional)
```

## Summary

Generates a binary tree with n vertices.

The returned view owns its underlying container.


#### Parameters
* *n*: Number of vertices in the generated graph. 


* *bidirectional*: True to add back-edges in a directed graph, false for forward edges only. 





#### Returns
A view over the generated graph. 


**See also**: make_binary_tree 

> sgl-reference


![https://i.imgur.com/lZSDjES.png](https://i.imgur.com/lZSDjES.png)

#### Example
```cpp
  using view_type = stapl::graph_view<stapl::multidigraph<int>>;

  auto v = stapl::generators::make_binary_tree<view_type>(256);
```
