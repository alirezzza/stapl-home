# make_star

Defined in <`stapl/containers/graph/generators/star.hpp`>

```c++
template<typename GraphView>  
GraphView make_star(std::size_t n,std::size_t r,bool bidirectional)
```

## Summary

Generates a star graph of size n, with root r.

The star graph will have the property that all of the vertices have an edge to a single particular vertex, designated the root. If the graph generated is not bidirectional, all non-root vertices will have a directed edge to the root and the root will have no outgoing edges.

The returned view owns its underlying container.


#### Parameters
* *n*: Size of the star. 


* *r*: The descriptor of the vertex that is the root. 


* *bidirectional*: Edges should be added in both directions





#### Returns
The original view, now containing the generated graph.

#### Example
```cpp
  using view_type = stapl::graph_view<stapl::multidigraph<int>>;

  auto v = stapl::generators::make_star<view_type>(256, 0, true);
```
