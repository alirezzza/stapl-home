# make_erdos_renyi

Defined in <`stapl/containers/graph/generators/erdos_renyi.hpp`>

```c++
template<typename GraphView>  
GraphView make_erdos_renyi(size_t size,double prob,bool bidirectional)
```

## Summary

Generates an Erdos-Renyi random network.

The generated Erdos-Renyi random network has the property that the probability of adding an edge u -> v is p, where u > v.

The returned view owns its underlying container.


#### Parameters
* *size*: Number of vertices in the output graph. 


* *prob*: The probability of adding any given edge. 


* *bidirectional*: True to add back-edges in a directed graph, false for forward edges only. 





#### Returns
A view over the generated graph.

#### Example
```cpp
  using view_type = stapl::graph_view<stapl::multidigraph<int>>;

  auto v = stapl::generators::make_erdos_renyi<view_type>(256, 0.2, true);
```
