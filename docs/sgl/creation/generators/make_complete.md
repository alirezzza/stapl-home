# make_complete

Defined in <`stapl/containers/graph/generators/complete.hpp`>

```c++
template<typename GraphView>  
GraphView make_complete(size_t n,bool bidirectional)
```

## Summary

Generate a complete graph.

The generated graph will have n vertices and all vertices will have an edge to all of the other vertices.

The returned view owns its underlying container.


#### Parameters
* *n*: Number of vertices in the output graph. 


* *bidirectional*: True to add back-edges in a directed graph, false for forward edges only. 





#### Returns
A view over the generated graph.


![https://i.imgur.com/cOFsYbf.png](https://i.imgur.com/cOFsYbf.png)

#### Example
```cpp
  using view_type = stapl::graph_view<stapl::multidigraph<int>>;

  auto v = stapl::generators::make_complete<view_type>(6, true);
```
