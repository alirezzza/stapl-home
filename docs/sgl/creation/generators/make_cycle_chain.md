# make_cycle_chain

Defined in <`stapl/containers/graph/generators/cycles.hpp`>

```c++
template<typename GraphView>  
GraphView make_cycle_chain(size_t cycle_count,size_t cycle_size,bool bidirectional)
```

## Summary

Generates a graph containing many linked cycles.

The graph will contain cycle_count cycles, each of which has cycle_size vertices. The largest vertex of each cycle is connected to the smallest vertex of the next cycle.

Results in cycle_count SCCs and one weakly-connected component in a directed graph. Results in a single CC in an undirected graph.

The returned view owns its underlying container.


#### Parameters
* *cycle_count*: Number of cycles in the generated graph. 


* *cycle_size*: Number of vertices in each cycle. 


* *bidirectional*: True to add back-edges in a directed graph, false for forward edges only. 





#### Returns
A view over the generated graph.

#### Example
```cpp
```
