# make_newman_watts_strogatz

Defined in <`stapl/containers/graph/generators/watts_strogatz.hpp`>

```c++
template<typename GraphView>  
GraphView make_newman_watts_strogatz(size_t n,size_t k,double p,bool bidirectional)
```

## Summary

Generate a Newman-Watts-Strogatz small-world graph [^1].

First create a ring over n nodes. Then each node in the ring is connected with its k nearest neighbors (k-1 neighbors if k is odd). Then shortcuts are created by adding new edges as follows: for each edge u-v in the underlying "n-ring with k nearest neighbors" with probability p add a new edge u-w with randomly-chosen existing node w. In contrast with Watts-Strogatz, no edges are removed.

The returned view owns its underlying container.


#### Parameters
* *n*: The number of nodes in the graph 


* *k*: Each node is connected to k nearest neighbors in ring topology 


* *p*: The probability of rewiring each edge 


* *bidirectional*: True for adding back-edges, False for forward only. 





#### Returns
A view over the generated graph. 


**See also**: make_watts_strogatz

#### Example
```cpp
  using view_type = stapl::graph_view<stapl::multidigraph<int>>;

  auto v = stapl::generators::make_newman_watts_strogatz<view_type>(
    128, 4, 0.2, true
  );
```
 [^1] M. E. J. Newman and D. J. Watts, Renormalization group analysis of the small-world network model, Physics Letters A, 263, 341, 1999. [http://dx.doi.org/10.1016/S0375-9601(99)00757-4](http://dx.doi.org/10.1016/S0375-9601(99)00757-4)
