# make_random_dag

Defined in <`stapl/containers/graph/generators/random_dag.hpp`>

```c++
template<typename GraphView>  
GraphView make_random_dag(size_t n)
```

## Summary

Generates a random directed acyclic graph (DAG).

The generated random directed acyclic graph will contain n vertices and potentially multiple components.

The returned view owns its underlying container.


#### Parameters
* *n*: Number of vertices in the generated graph. 





#### Returns
A view over the generated graph.

#### Example
```cpp
  using view_type = stapl::graph_view<stapl::multidigraph<int>>;

  auto v = stapl::generators::make_random_dag<view_type>(256);
```
