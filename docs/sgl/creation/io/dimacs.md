# Matrix Market

SGL provides facilities to read and write the [DIMACS](http://prolland.free.fr/works/research/dsat/dimacs.html) format.

## Format

Note that indices are one based.

```
c comments
p sp #vertices #edges
a source0 target0
a source1 target1
:
```

## Reading

To read a DIMACS file, call the function as follows:

```c++
graph_view<Graph> read_dimacs(std::string filename, size_t blk_sz=4096);
```

## Writing

To write a graph to a file in the DIMACS format, use:

```c++
write_dimacs(G g, std::string filename);
```
