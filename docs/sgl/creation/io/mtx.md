# Matrix Market

SGL provides facilities to read and write the [Matrix Market](http://math.nist.gov/MatrixMarket/formats.html) format.

## Format

Note that indices are one based.

```
This ASCII file represents a sparse MxN matrix with L
nonzeros in the following Matrix Market format:

+----------------------------------------------+
|%%MatrixMarket matrix coordinate real general | <--- header line
|%                                             | <--+
|% comments                                    |    |-- 0 or more comment lines
|%                                             | <--+         
|    M  N  L                                   | <--- rows, columns, entries
|    I1  J1  A(I1, J1)                         | <--+
|    I2  J2  A(I2, J2)                         |    |
|    I3  J3  A(I3, J3)                         |    |-- L lines
|        . . .                                 |    |
|    IL JL  A(IL, JL)                          | <--+
+----------------------------------------------+   
```

## Reading

To read a Matrix Market file, call the function as follows:

```c++
graph_view<Graph> read_matrix_market(std::string filename, size_t blk_sz=4096);
```

## Writing

To write a graph to a file in the Matrix Market format, use:

```c++
write_matrix_market(G g, std::string filename);
```
