# Graph I/O

SGL provides a generic graph I/O framework to read/write graphs from files. Users can define how to read a graph from file by writing an operator that decodes what a single line form the file should be parsed as. Similarly, users can write an operator to serialize a vertex and its adjacent edges to their file-format.

Using this, SGL provides readers and writers for several common graph file formats (adjacency-list, edge-list, dimacs, dot, shards, etc.) Reading a graph from a text file storing an edge-list can be done as follows:

```c++
graph_view<graph_type> graph_i = read_edge_list<graph_type>("input.graph");
```

Similarly, a graph can be written to a file in the edge-list format as follows:

```c++
write_edge_list(graph_i, "graph.out");
```

Graphs can be written or read to/from other formats in a similar way.
