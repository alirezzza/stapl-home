# Edge List

SGL provides facilities to read and write the common edge list format.

## Format

The first line of the input is the size of the graph in terms of the number of
vertices and the number of edges.
Every line after that will contain a single edge which will be of the format `source target`.
If edges are weighted, the line format becomes `source target weight`. For example,
an unweighted graph with 3 vertices and 4 edges will look like:

```
3 4
0 1
1 2
2 0
2 1
```

## Reading

To read an edge list from a file, call the function as follows:

```c++
graph_view<Graph> read_edge_list(std::string filename, size_t blk_sz=4096);
```

## Writing

To write a graph to a file in the edge list format, use:

```c++
write_edge_list(G g, std::string filename);
```
