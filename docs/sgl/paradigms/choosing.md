# Choosing the Right Paradigm

This section will include qualitative advise on how to select an appropriate paradigm.

## Fine-Grained
If the algorithm is easily and naturally expressible in a fine-grained (vertex-centric) manner, we should express it using the vertex- and neighbor-operators as show in the previous sections. From here, we can use the following rules-of-thumb:

* If graph does not fit in available system RAM ⇒ use the Hybrid Out-of-Core Paradigm. This combines hierarchical and asynchronous execution with out-of-core processing.
* If graph fits in RAM:
  * If graph exhibits Power-law distribution, or has a small diameter with high out-degree vertices ⇒ use the Hierarchical Communication Reduction (HCR) Paradigm.
  * Otherwise use the KLA Paradigm:
* If graph has a large diameter ⇒ k>1.
* If optimal k is unknown, use the adaptive KLA paradigm.

## Hierarchical
If the algorithm is naturally expressible hierarchically, we can use the Hierarchical Paradigm.
